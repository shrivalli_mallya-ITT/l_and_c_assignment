package com.intimetec.atmsimulator.vo;

public class User {
	private int atmPin;
	private String mobileNumber;
	private long accountBalance;

	public User(int atmPin, String mobileNumber, long accountBalance) {
		this.atmPin = atmPin;
		this.mobileNumber = mobileNumber;
		this.accountBalance = accountBalance;
	}

	public int getAtmPin() {
		return atmPin;
	}

	public void setAtmPin(int atmPin) {
		this.atmPin = atmPin;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public long getAccountBalance() {
		return accountBalance;
	}
	
	public void setAccountBalance(long accountBalance) {
		this.accountBalance = accountBalance;
	}
}
