package com.intimetec.atmsimulator.vo;

public class ATM {
	private int atmBalance;
	
	public ATM(int atmBalance) {
		this.atmBalance = atmBalance;	
	}

	public int getAtmBalance() {
		return atmBalance;
	}

	public void setAtmBalance(int atmBalance) {
		this.atmBalance = atmBalance;
	}
}