package com.intimetec.factory;

import com.intimetec.auth.Authentication;
import com.intimetec.auth.impl.AuthenticationViaOtp;
import com.intimetec.auth.impl.AuthenticationViaUserId;

public class AuthenticationFactoryConcrete implements AuthenticationFactory {

	@Override
	public Authentication getAuthentication(int userChoice) {

		Authentication authentication = null;
		switch (userChoice) {
		case 1:
			authentication = new AuthenticationViaUserId();
			break;
		case 2:
			authentication = new AuthenticationViaOtp();
			break;
		default:
			throw new NullPointerException();

		}
		return authentication;
	}

}
