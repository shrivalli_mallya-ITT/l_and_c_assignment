package com.intimetec.auth.impl;

import java.util.Scanner;

import com.intimetec.auth.Authentication;

public class AuthenticationViaOtp implements Authentication {

	@Override
	public void loginToWallet() {

		String mobileNumber=null, otp=null;
		System.out.println("Enter mobile number:");
		Scanner scan = new Scanner(System.in);
		mobileNumber = scan.next();
		System.out.println("Enter the otp:");
		otp = scan.next();
		System.out.println("USER LOGGED IN THROUGH MOBILE NUMBER AND OTP");
	}
}
