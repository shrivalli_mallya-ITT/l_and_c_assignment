package com.intimetec.atmsimulator.exception;

public class InsufficentAccountBalanceException extends Exception {

	public InsufficentAccountBalanceException(String message) {
		super(message);
	}
}
