package com.intimetec.main;

import java.util.Scanner;

import com.intimetec.auth.Authentication;
import com.intimetec.auth.impl.AuthenticationViaOtp;
import com.intimetec.auth.impl.AuthenticationViaUserId;
import com.intimetec.factory.AuthenticationFactory;
import com.intimetec.factory.AuthenticationFactoryConcrete;

public class AuthMain {

	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.println("SELECT YOUR CHOICE OF LOGIN ");
		System.out.println("ENTER 1: USERID and PASSWORD LOGIN.");
		System.out.println("ENTER 2: MOBILE NUMBER and OTP LOGIN.");
		int choice = scan.nextInt();

		AuthenticationFactory factory = new AuthenticationFactoryConcrete();

		try {
			Authentication authentication = factory.getAuthentication(choice);
			authentication.loginToWallet();
		} catch (NullPointerException e) {
			System.out.println("Please enter a valid choice!");
		}
	}
}
