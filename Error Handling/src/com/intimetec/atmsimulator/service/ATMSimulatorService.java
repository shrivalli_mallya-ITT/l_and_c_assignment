package com.intimetec.atmsimulator.service;

import com.intimetec.atmsimulator.exception.InsufficentAccountBalanceException;
import com.intimetec.atmsimulator.exception.InsufficientAtmBalanceException;

public interface ATMSimulatorService {

	public void userInputHandler(int userInput);

	public void withdrawAmount(int amount) throws InsufficentAccountBalanceException, InsufficientAtmBalanceException;

	public void checkBalance();

	public void changeATMPin();

	public void changeMobileNumber();

	public boolean authenticateUser(int atmPin);
}