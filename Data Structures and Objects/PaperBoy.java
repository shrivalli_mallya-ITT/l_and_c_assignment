public class PaperBoy {

	private float payment;

	public void recievePayment(Customer customer) {
		payment = (float) 7.00;
		double paidAmount = customer.getPayment(payment);
		if (paidAmount == payment) {
			System.out.println("Thanks for the payment!");
		} else {
			System.out.println("The payment is not full, I will come back later and collect the money!");
		}
	}

	public static void main(String[] args) {
		Customer customer = new Customer();
		customer.setWallet();
		PaperBoy paperBoy = new PaperBoy();
		paperBoy.recievePayment(customer);
	}
}
