package com.intimetec.atmsimulator.service;

import com.intimetec.atmsimulator.constants.ATMSimulatorConstants;
import com.intimetec.atmsimulator.exception.InsufficentAccountBalanceException;
import com.intimetec.atmsimulator.exception.InsufficientAtmBalanceException;
import com.intimetec.atmsimulator.vo.ATM;
import com.intimetec.atmsimulator.vo.User;

public class ATMSimulatorServiceImpl implements ATMSimulatorService {

	ATM atm = new ATM(2500000);
	User user = new User(1111, "9898909890", 15000);

	@Override
	public void userInputHandler(int userInput) {

		switch (userInput) {
		case 1: {
			int withdrawlAmount;
			do {
				System.out.println(ATMSimulatorConstants.WITHDRAW_AMOUNT_MESSAGE);
				withdrawlAmount = ATMSimulatorConstants.scan.nextInt();
			} while (withdrawlAmount % 100 != 0);
			try {
				withdrawAmount(withdrawlAmount);
			} catch (InsufficentAccountBalanceException e) {
				System.out.println(e.getMessage());
			} catch (InsufficientAtmBalanceException e) {
				System.out.println(e.getMessage());
			}
			break;
		}
		case 2: {
			checkBalance();
			break;
		}
		case 3: {
			changeATMPin();
			break;
		}
		case 4: {
			changeMobileNumber();
			break;
		}

		default: {
			System.out.println(ATMSimulatorConstants.SESSION_EXPIRED);
			System.exit(0);
		}
		}

	}

	@Override
	public void withdrawAmount(int amount) throws InsufficentAccountBalanceException, InsufficientAtmBalanceException {
		if (user.getAccountBalance() > amount) {
			if (atm.getAtmBalance() > amount) {
				user.setAccountBalance(user.getAccountBalance() - amount);
				System.out.println(ATMSimulatorConstants.COLLECT_CASH);
				System.out.println(ATMSimulatorConstants.NEW_BALANCE + user.getAccountBalance());
			} else {
				throw new InsufficientAtmBalanceException(ATMSimulatorConstants.INSUFFICIENT_ATM_BALANCE);
			}
		} else {
			throw new InsufficentAccountBalanceException(ATMSimulatorConstants.INSUFFICIENT_ACCOUNT_BALANCE);
		}
	}

	@Override
	public void checkBalance() {
		System.out.println(ATMSimulatorConstants.CURRENT_BALANCE + user.getAccountBalance());
	}

	@Override
	public void changeATMPin() {
		System.out.println(ATMSimulatorConstants.ENTER_OLD_PIN);
		int oldAtmPin = ATMSimulatorConstants.scan.nextInt();
		if (authenticateUser(oldAtmPin)) {
			System.out.println(ATMSimulatorConstants.ENTER_NEW_PIN);
			int newAtmPin = ATMSimulatorConstants.scan.nextInt();
			user.setAtmPin(newAtmPin);
			System.out.println(ATMSimulatorConstants.PIN_CHANGED_SUCCESSFULLY);
		}
	}

	@Override
	public void changeMobileNumber() {
		System.out.println(ATMSimulatorConstants.ENTER_PIN);
		int atmPin = ATMSimulatorConstants.scan.nextInt();
		if (authenticateUser(atmPin)) {
			System.out.println(ATMSimulatorConstants.ENTER_NEW_MOBILENUMBER);
			String newMobileNumber = ATMSimulatorConstants.scan.next();
			user.setMobileNumber(newMobileNumber);
			System.out.println(ATMSimulatorConstants.MOBILENUMBER_CHANGED_SUCCESSFULLY);
		}
	}

	@Override
	public boolean authenticateUser(int atmPin) {
		if (atmPin == user.getAtmPin()) {
			return true;
		}
		return false;
	}
}
