public class Wallet {
	private double totalMoney = 0.0;

	public double getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(double newValue) {
		totalMoney = newValue;
	}

	public void addMoney(double deposit) {
		totalMoney += deposit;
	}

	public void subtractMoney(double payment) {
		totalMoney -= payment;
	}
}
