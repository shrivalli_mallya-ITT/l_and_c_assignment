public class Customer {
	private String firstName;
	private String lastName;
	private Wallet wallet;

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setWallet() {
		wallet = new Wallet();
		wallet.setTotalMoney(5);
	}

	public double getPayment(double payment) {
		if (wallet.getTotalMoney() != 0.0) {
			if (wallet.getTotalMoney() > payment) {
				wallet.subtractMoney(payment);
			} else {
				payment = wallet.getTotalMoney();
			}
		} else {
			payment = 0.0;
		}
		return payment;
	}
}