package com.intimetec.atmsimulator.exception;

public class InsufficientAtmBalanceException extends Exception {

	public InsufficientAtmBalanceException(String message) {
		super(message);
	}
}
