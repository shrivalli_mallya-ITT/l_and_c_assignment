package com.intimetec.factory;

import com.intimetec.auth.Authentication;

public interface AuthenticationFactory {
	
	public Authentication getAuthentication(int userChoice);
}
