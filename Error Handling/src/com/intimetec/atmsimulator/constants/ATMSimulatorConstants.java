package com.intimetec.atmsimulator.constants;

import java.util.Scanner;

public class ATMSimulatorConstants {

	public static final Scanner scan = new Scanner(System.in);
	public static final String INSERT_CARD = "Please insert the card!";
	public static final String ENTER_PIN = "Enter your 4-digit atm pin:";
	public static final String WITHDRAW_AMOUNT = "1.Withdraw Amount";
	public static final String CHECK_BALANCE = "2.Check balance";
	public static final String CHANGE_PIN = "3.Change ATM pin";
	public static final String CHANGE_MOBILENUMBER = "4.Change registered mobile number";
	public static final String CARD_BLOCK = "Your card is blocked!";
	public static final String WITHDRAW_AMOUNT_MESSAGE = "Enter the withdrawl amount in multiple of 100 or 500";
	public static final String SERVER_DOWN = "Server is down";
	public static final String SESSION_EXPIRED = "Your session has expired";
	public static final String INSUFFICIENT_ACCOUNT_BALANCE = "Insufficient account balance";
	public static final String INSUFFICIENT_ATM_BALANCE = "Insufficient ATM balance";
	public static final String ENTER_NEW_PIN = "Enter the new atm pin:";
	public static final String ENTER_OLD_PIN = "Enter your old atm pin:";
	public static final String COLLECT_CASH = "Please collect the cash";
	public static final String CURRENT_BALANCE = "Your current account balance:";
	public static final String NEW_BALANCE = "New balance:";
	public static final String ENTER_NEW_MOBILENUMBER = "Enter the new mobile number:";
	public static final String PIN_CHANGED_SUCCESSFULLY = "Your atm pin was changed successfully!";
	public static final String MOBILENUMBER_CHANGED_SUCCESSFULLY = "Your mobile number was changed successfully!";
	public static final int MAX_USER_ATTEMPTS = 3;
}