package com.tumblr.tumblrService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/*
 * @author shrivalli.mallya
 */

public class TumblrService {

	public static String inputBlogName(Scanner scan) {
		System.out.println("Enter the Tumblr blog name:");
		String blogName = scan.nextLine();
		return blogName;
	}

	public static String inputPostRange(Scanner scan) {
		System.out.println("Enter the range:");
		String postRange = scan.nextLine();
		return postRange;
	}

	public static int getStartRange(String range) throws NumberFormatException {
		String[] postRange = range.split("-");
		int startRange = Integer.parseInt((postRange[0]));
		if (startRange > 0) {
			startRange = startRange - 1;
		}
		return startRange;
	}

	public static int getNumberOfPosts(String range) throws NumberFormatException {
		String[] postRange = range.split("-");
		int numberOfPosts = Integer.parseInt(postRange[1]);
		return numberOfPosts;
	}

	public static boolean validateNumberOfPosts(int numberOfPosts) {
		return numberOfPosts < 50;
	}

	public static String sendGetRequest(String blogName, int startRange, int numberOfPosts) throws IOException {

		StringBuffer response = new StringBuffer();
		URL url = new URL("https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + numberOfPosts
				+ "&start=" + startRange);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		int responseCode = connection.getResponseCode();
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			// getting input from connection object
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		}
		return response.toString();
	}

	// modifying the response in order to parse it into a JSON object
	public static String trimResponse(String response) {
		String[] responseArray = response.split("var tumblr_api_read ="); // eliminating 'var tumblr_api_read =' from
																			// the response
		int lastIndexOfSemicolon = responseArray[1].lastIndexOf(";");
		String modifiedResponse = responseArray[1].substring(0, lastIndexOfSemicolon); // eliminating ';' at the end of
																						// the response
		return modifiedResponse;
	}

	public static JSONObject parseResponseIntoJson(String newResponse) {
		JSONParser parse = new JSONParser();
		JSONObject jsonResponse = null;
		try {
			jsonResponse = (JSONObject) parse.parse(newResponse);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	public static void fetchDesiredResponse(JSONObject jsonResponse, String postRange) {
		String title = (String) jsonResponse.get("title");
		Long numberOfPosts = (Long) jsonResponse.get("posts-total");
		Object tumbleLog = jsonResponse.get("tumblelog");
		String description = (String) ((HashMap) tumbleLog).get("description");
		String name = (String) ((HashMap) tumbleLog).get("name");
		System.out.println("Title: " + title + "\nName: " + name + "\nDescription" + description + "\nNumber of Posts: "
				+ numberOfPosts + "\n");
		JSONArray posts = (JSONArray) jsonResponse.get("posts");
		Iterator<JSONObject> postIterator = posts.iterator();
		String[] range = postRange.split("-");
		int postNumber = Integer.parseInt(range[0]);
		while (postIterator.hasNext()) {
			JSONObject post = postIterator.next();
			JSONArray photos = (JSONArray) post.get("photos");
			int imageCount = 0; // count of photos in a single post
			if (!photos.isEmpty()) {
				Iterator<JSONObject> photoIterator = photos.iterator();
				while (photoIterator.hasNext()) {
					JSONObject photo = photoIterator.next();
					String imageUrl = (String) photo.get("photo-url-1280");
					if (imageCount == 0) {
						System.out.println(postNumber + "." + imageUrl);
						++imageCount;
					} else {
						System.out.println("  " + imageUrl);
					}
				}
			} else {
				System.out.println(postNumber + "." + post.get("photo-url-1280"));
			}
			++postNumber;
		}
	}

	public static void terminatingExecution() {
		System.exit(0);
	}

	public static void main(String[] args) throws IOException {
		Scanner scan = new Scanner(System.in);
		String blogName = inputBlogName(scan);
		String postRange = inputPostRange(scan);
		int startRange = 0, numberOfPosts = 0;
		try {
			startRange = getStartRange(postRange);
			numberOfPosts = getNumberOfPosts(postRange);
		} catch (NumberFormatException numberFormatException) {
			System.out.println("Entered range format is invalid!");
			terminatingExecution();
		}
		boolean isValidNumberOfPosts = validateNumberOfPosts(numberOfPosts);
		if (isValidNumberOfPosts) {
			String response = sendGetRequest(blogName, startRange, numberOfPosts);
			if (!response.equals("")) {
				String modifiedResponse = trimResponse(response);
				JSONObject jsonResponse = parseResponseIntoJson(modifiedResponse);
				fetchDesiredResponse(jsonResponse, postRange);
			} else {
				System.out.println("Entered blog name is invalid!");
			}
		} else {
			System.out.println("Range should not exceed 50");
		}
	}
}