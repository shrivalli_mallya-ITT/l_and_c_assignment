package com.intimetec.atmsimulator.main;

import com.intimetec.atmsimulator.constants.ATMSimulatorConstants;
import com.intimetec.atmsimulator.service.ATMSimulatorService;
import com.intimetec.atmsimulator.service.ATMSimulatorServiceImpl;
import com.intimetec.server.Server;

public class ATMSimulatorApplication {

	public static void main(String[] args) {
		int userAttempts = 0;
		System.out.println(ATMSimulatorConstants.INSERT_CARD);
		while (userAttempts < ATMSimulatorConstants.MAX_USER_ATTEMPTS) {
			Server server = new Server();
			if (server.connectToServer()) {
				System.out.println(ATMSimulatorConstants.ENTER_PIN);
				int atmPin = ATMSimulatorConstants.scan.nextInt();
				ATMSimulatorService atmSimulatorService = new ATMSimulatorServiceImpl();
				boolean isValidUser = atmSimulatorService.authenticateUser(atmPin);
				if (isValidUser) {
					System.out.println(ATMSimulatorConstants.WITHDRAW_AMOUNT);
					System.out.println(ATMSimulatorConstants.CHECK_BALANCE);
					System.out.println(ATMSimulatorConstants.CHANGE_PIN);
					System.out.println(ATMSimulatorConstants.CHANGE_MOBILENUMBER);
					int userInput = ATMSimulatorConstants.scan.nextInt();
					atmSimulatorService.userInputHandler(userInput);
					break;
				} else {
					++userAttempts;
				}
			} else {
				System.out.println(ATMSimulatorConstants.SERVER_DOWN);
				break;
			}
		}

		if (userAttempts == ATMSimulatorConstants.MAX_USER_ATTEMPTS) {
			System.out.println(ATMSimulatorConstants.CARD_BLOCK);
		}
	}
}